﻿using ProductConsoleApp.Reposiotry;
using System;
using Xunit;

namespace ConsoleCoreApp.Tests
{
    public class ProductRepositoryTeset
    {
        ProductReposiotry productReposiotry;
        public ProductRepositoryTeset()
        {
            productReposiotry = new ProductReposiotry();
        }

        [Fact]
        public void GetAllProductReturnsProductArray()
        {
            string expectedProductName = "LG";


            //Act
            var products= productReposiotry.GetAllProducts();
            var actualProductName = products[0].Name;//LG


            //Assert
            Assert.Equal(expectedProductName, actualProductName);
        }
    }
}
