﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProductConsoleApp.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsAvailable { get; set; } = true;

        public override string ToString()
        {
            return $"Product Id::{Id}\t {Name}\t{IsAvailable}";
        }
    }
}
